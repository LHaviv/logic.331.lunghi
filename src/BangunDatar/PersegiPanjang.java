package BangunDatar;
import java.util.Scanner;

public class PersegiPanjang {

    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas()
    {
        // Luas Persegi Panjang
        System.out.println("Input panjang (cm)");
        panjang = input.nextInt();

        System.out.println("Input lebar (cm)");
        lebar = input.nextInt();

        int luas = panjang * lebar;

        System.out.println("Luas persegi panjang adalah = " + luas);
    }

    public static void Keliling()
    {
        // Luas Persegi Panjang
        System.out.println("Input panjang (cm)");
        panjang = input.nextInt();

        System.out.println("Input lebar (cm)");
        lebar = input.nextInt();

        int keliling = (2 * panjang) + (2 * lebar);

        System.out.println("Luas persegi panjang adalah = " + keliling);

    }
}
