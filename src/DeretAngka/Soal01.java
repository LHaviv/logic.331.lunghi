package DeretAngka;

public class Soal01 {
    public static void Resolve(int n)
    {
        int ganjil = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = ganjil;
            ganjil += 2;
        }

        Utility.PrintArray1D(results);

    }
}
