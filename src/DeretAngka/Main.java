package DeretAngka;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih Soal (1 - 12)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        System.out.println("Masukan nilai n: ");
        n = input.nextInt();

        switch (pilihan)
        {
            case 1:
                Soal01.Resolve(n);
                break;
            case 2:
                break;
            default:

        }
    }
}