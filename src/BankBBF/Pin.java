package BankBBF;

import java.util.Scanner;

public class Pin {

    private static Scanner input = new Scanner(System.in);
    public static String BuatPin()
    {
        String buatPin;

        System.out.println("Buat buatPin");
        buatPin = input.nextLine();

        while (buatPin.length() != 6)
        {
            System.out.println("Pin harus 6 digit");
            buatPin = input.nextLine();
        }
        while (!Utility.IsNumeric(buatPin))
        {
            System.out.println("Pin harus angka");
            buatPin = input.nextLine();
        }

        return buatPin;
    }

    public static boolean MasukanPin(String buatPin)
    {
        String masukanPin;
        int count = 0;
        boolean flag = true;

        System.out.println("Masukan buatPin");
        masukanPin = input.nextLine();

        while (!masukanPin.equals(buatPin) && count < 3) {
            System.out.println("Pin salah. enter lagi");
            masukanPin = input.nextLine();
            count ++;
        }

        if (count == 3)
        {
            System.out.println("ATM diblokir");
            flag = false;
        }

        return flag;
    }
}
